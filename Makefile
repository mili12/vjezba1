NAME=hello

all: $(NAME)

$(NAME): $(NAME).c
		gcc -Wall -o $(NAME) $(NAME).c

run:
	./$(NAME)
		
clean:
	rm -rf $(NAME) $(NAME)