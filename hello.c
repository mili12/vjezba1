#include <stdio.h>


int add(int a, int b)
{
  return a + b;
}

int minus(int a, int b)
{
  return a - b;
}

int main()
{
  printf("Hello world\n");
  int a = 5, b = 6;
  int c = add(a, b);
  printf("Variable c = %d\n", c);
  
  return 0;
}
